﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Exanite.UDCWeek1.EntitySpawner;

public class GameController : MonoBehaviour 
{
	public static GameController Instance;
	public EntitySpawnTableSO entitySpawnTableSO;
	public int level = 0;
	public List<GameObject> levels;

	public List<GameObject> enemies;
	public float timeOfLastCheck = -2f;

	public GameObject currentLevel;

	protected Entity player;

	private void Awake() 
	{
		if(!Instance)
		{
			Instance = this;
		}
		else
		{
			Debug.LogError(string.Format("There is already a {0} in the scene.", GetType()));
		}
	}

	private void Start() 
	{
		SceneManager.sceneLoaded += OnSceneLoaded;

		player = GameObject.Find("Player").GetComponent<Entity>();
	}

	private void Update() 
	{
		// if(Input.GetKey(KeyCode.Space))// && Input.GetKey(KeyCode.R))
		// {
		// 	Debug.Log("resetting scene");
		// 	SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		// }

		if(Time.time - timeOfLastCheck > 0.25)
		{
			enemies.RemoveAll(enemy => enemy.activeSelf == false);

			if(GameController.Instance.enemies.Count == 0)
        	{
            	NextLevel();
        	}
		}
	}

	private void NextLevel()
	{
		player.health.Value += player.health.Max.FinalValue / 4;
		//level++;
		level = Mathf.Clamp(level + 1, 0, entitySpawnTableSO.levels.Count);
		
		Transform levelEnd = currentLevel.transform.Find("End");

		GameObject nextLevel = levels[Random.Range(0, levels.Count)];
		Transform nextLevelStart = nextLevel.transform.Find("Start");
		Vector3 offset = new Vector3(-nextLevelStart.localPosition.x, -nextLevelStart.localPosition.y, 0);

		currentLevel = Instantiate(nextLevel, levelEnd.position + offset, Quaternion.identity);

		nextLevelStart = currentLevel.transform.Find("Start");
		levelEnd.position = new Vector3(levelEnd.position.x, levelEnd.position.y, 0.95f);
		nextLevelStart.position = new Vector3(nextLevelStart.position.x, nextLevelStart.position.y, 0.95f);
	}

	private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
	{
		Time.timeScale = 1.0f;
		Time.fixedDeltaTime = Time.timeScale * 0.02f;
		level = 0;
		enemies.Clear();
		timeOfLastCheck = 0f;
	}

	public IEnumerator SlowDownAndRestart()
    {
        float realtimePassed = 0f;
        while(Time.timeScale != 0.1f)
        {
            Time.timeScale = Mathf.SmoothStep(Time.timeScale, 0.1f, realtimePassed/5f);
            realtimePassed += Time.unscaledDeltaTime;
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
            yield return null;
        }
        yield return new WaitForSecondsRealtime(1f);
        Time.timeScale = 1f;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
