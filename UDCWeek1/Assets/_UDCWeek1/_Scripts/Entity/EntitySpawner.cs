﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.UDCWeek1.EntitySpawner;
using Exanite.ObjectPooling;

public class EntitySpawner : MonoBehaviour 
{
	public EntitySpawnTableSO spawnTable;

	private void Start()
	{
		if(GameController.Instance.level > 0)
		{
			SpawnEntity(GetEntityToSpawn());
		}
	}

	private GameObject GetEntityToSpawn()
	{
		float highEnd = 0f;

		foreach(Spawn spawn in spawnTable.levels[GameController.Instance.level - 1].spawns)
		{
			highEnd += spawn.chance;
		}

		float roll = Random.Range(0f, highEnd);
		//Debug.Log(roll);
		float cumulative = 0f;

		foreach(Spawn spawn in spawnTable.levels[GameController.Instance.level - 1].spawns)
		{
			if(roll >= cumulative && roll < (cumulative + spawn.chance))
			{
				return spawn.enemyPrefab;
			}

			cumulative += spawn.chance;
		}

		throw new System.Exception(string.Format("{0} failed to return a prefab to spawn", this));
	}

	private void SpawnEntity(GameObject entity)
	{
		if(entity == null) return;
		GameController.Instance.enemies.Add(Pool.Spawn(entity, transform.position, transform.rotation));
	}
}
