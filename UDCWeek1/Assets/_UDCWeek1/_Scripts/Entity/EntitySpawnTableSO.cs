﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Exanite.UDCWeek1.EntitySpawner
{
	[CreateAssetMenu(fileName = "EntitySpawnerSO", menuName = "UDCWeek1/EntitySpawnerSO", order = 0)]
	public class EntitySpawnTableSO : ScriptableObject 
	{
		public List<EntitySpawn> levels;
	}

	[Serializable]
	public class EntitySpawn
	{
		public List<Spawn> spawns;
	}

	[Serializable]
	public class Spawn
	{
		public GameObject enemyPrefab;
		public float chance;
	}
}