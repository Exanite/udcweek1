﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Entity))]
public class HealthBar : MonoBehaviour 
{
	public GameObject healthbar;
	public Vector3 offset;

	protected Transform foreground;

	protected Entity entity;

	private void Start() 
	{
		entity = GetComponent<Entity>();
		healthbar = Instantiate(healthbar, transform);
		foreground = healthbar.transform.Find("Foreground");
	}

	private void Update() 
	{
		healthbar.transform.position = transform.position + offset;
		foreground.localScale = new Vector3(entity.health.Value / entity.health.Max.FinalValue, 1, 1);
	}
}
