﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class PlayerCameraController : MonoBehaviour 
{
	public GameObject target;
	public Vector3 offset;

	public float damping = 0.1f;
	public float maxDistance = 10f;
	[Range(0f, 1f)]public float mouseOffsetStrength = 0.1f;

	private Vector3 _currentVelocity;
	private EntityPlayer m_entityPlayer;

	private void Start()
	{
		target = (target == null) ? GameObject.FindWithTag("Player") : target;
		m_entityPlayer = (target != null) ? target.GetComponent<EntityPlayer>() : null;
	}

	private void FixedUpdate() 
	{
		if(target)
		{
			Vector3 mouseOffset = Vector3.Slerp(m_entityPlayer.mouseLocation - target.transform.position, Vector3.zero, 1 - mouseOffsetStrength);
			mouseOffset = Vector3.ClampMagnitude(mouseOffset, maxDistance);

			transform.position = Vector3.SmoothDamp(transform.position, target.transform.position + offset + mouseOffset, ref _currentVelocity, damping);
		}
		else
		{
			target = GameObject.FindWithTag("Player");
		}

		if(m_entityPlayer == null && target)
		{
			m_entityPlayer = target.GetComponent<EntityPlayer>();
		}
	}
}
