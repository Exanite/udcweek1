﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.Stats;
using Exanite.SkillSystem;
using UnityEngine.SceneManagement;
using Exanite.ObjectPooling;

public class EntityPlayer : Entity
{
    [Header("EntityPlayer:")]
    public Vector3 mouseLocation;
    public float speed = 5f;

    public GameObject crosshair;
    public GameObject weapon;
    public GameObject shootSound;

    protected Touch touch;

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update() 
    {
        base.Update();
        GetMouseLocation();
        
        crosshair.transform.position = mouseLocation;
        
        if(Input.anyKeyDown)
        {
            if(skillOne.Use(true))
            {
                Pool.Spawn(shootSound, transform.position, transform.rotation, transform);
            }
        }
        if(Input.anyKey)
        {
            m_rigidbody.useGravity = false;
            SetPlayerVelocity();
            if(skillOne.Use(false))
            {
                Pool.Spawn(shootSound, transform.position, transform.rotation, transform);
            }
        }
        else
        {
            m_rigidbody.useGravity = true;
        }
    }

    protected virtual void GetMouseLocation()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;

		if(Physics.Raycast(ray, out hit, 100f, 1 << 9))
		{
            Vector3 temp = new Vector3(hit.point.x, hit.point.y, 0f);
			mouseLocation = temp;
		}
    }

    protected virtual void SetPlayerVelocity()
    {
        Vector3 velocity = (Vector3.Distance(mouseLocation, transform.position) < speed) ? Vector3.ClampMagnitude((mouseLocation - transform.position) * 2f, speed) : (mouseLocation - transform.position).normalized * speed;
        
        m_rigidbody.velocity = velocity;
    }

    protected override void OnDeath()
    {
        GameController.Instance.StartCoroutine(GameController.Instance.SlowDownAndRestart());
        base.OnDeath();
    }
}