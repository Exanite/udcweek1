﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.ObjectPooling;

public class EntityStationaryTurret : EntityAI 
{
    public GameObject shootSound;

	protected override void Update() 
    {
        base.Update();
        
        if(skillOne.Use())
        {
            Pool.Spawn(shootSound, transform.position, transform.rotation, transform);
        }
    }
}
