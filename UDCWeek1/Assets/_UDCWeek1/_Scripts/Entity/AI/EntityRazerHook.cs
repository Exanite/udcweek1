﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.ObjectPooling;

public class EntityRazerHook : EntityAI
{
    [Header("EntityHookShot:")]
    public GameObject shootBulletSound;
    public GameObject bladeFront;
    public GameObject bladeBack;
    public GameObject hook;
    public float bladeSpinSpeed = 180;
    public float aimTime = 2;
    public float speed = 10;
    public float damagePerSec = 20f;
    public bool isElite = false;

    public LineRenderer laser;
    public Transform laserStart;
    public Transform laserEnd;
    public Transform laserHit;

    public HookState hookState = HookState.Inactive;
    public HookState nextHookState = HookState.Inactive;

    protected Coroutine updateAICoroutine;

    public enum HookState
    {
        Inactive,
        Aiming,
        Shooting,
    }

    protected override void Update() 
    {
        base.Update();

        if(target == null && updateAICoroutine == null && gameObject.activeSelf == true)
        {
            updateAICoroutine = StartCoroutine(UpdateAICoroutine());
        }

        if(target && updateAICoroutine != null)
        {
            StopCoroutine(updateAICoroutine);
            updateAICoroutine = null;
        }

        bladeFront.transform.Rotate(Vector3.forward * -bladeSpinSpeed * Time.deltaTime);
        bladeBack.transform.Rotate(Vector3.forward * bladeSpinSpeed * Time.deltaTime);

        laser.SetPosition(0, laserStart.position);
        laser.SetPosition(1, laserEnd.position);
        laserHit.LookAt(Camera.main.transform.position);

        if(target)
        {
            switch(hookState)
            {
                case(HookState.Inactive):
                    if(target && nextHookState == HookState.Inactive)
                    {
                        nextHookState = HookState.Aiming;
                        StartCoroutine(AimAtTarget(aimTime));
                    }
                    break;
                case(HookState.Aiming):
                    if(target && nextHookState == HookState.Aiming)
                    {
                        nextHookState = HookState.Shooting;
                        StartCoroutine(ShootAtTarget());
                    }
                    break;
            }
        }
    }

    protected virtual void FixedUpdate() 
    {
        laserHit.position = laserEnd.position + ((laserEnd.position - laserStart.position).normalized * 0.1f);
    }

    public override void OnSpawn()
    {
        base.OnSpawn();

        updateAICoroutine = StartCoroutine(UpdateAICoroutine());
    }

    public override void OnDespawn()
    {
        base.OnDespawn();

        hookState = HookState.Inactive;
        nextHookState = HookState.Inactive;
        laserEnd.transform.position = laserStart.transform.position;
        target = null;
        hook.transform.LookAt(hook.transform.position + Vector3.up);

        if(updateAICoroutine != null) StopCoroutine(updateAICoroutine);
        updateAICoroutine = null;
    }

    protected IEnumerator AimAtTarget(float aimTime)
    {
        float time = 0.0f;
        bool canSeeTarget = false;
        while(target != null && time < aimTime || !canSeeTarget)
        {
            RaycastHit hit;

            if(!canSeeTarget && target != null && Physics.Linecast(transform.position, target.transform.position, out hit)) 
            {
                if(hit.collider.name == target.name)
                {
                    canSeeTarget = true;
                }
            }

            if(canSeeTarget)
            {
                time += Time.deltaTime;
                hook.transform.LookAt(target.transform.position);
                Ray ray = new Ray(transform.position, laserStart.position - transform.position);

                if(Physics.Raycast(ray, out hit, searchRange * 10, ~(1 << 11)))
                {
                    laserEnd.position = Vector3.Lerp(laserEnd.position, hit.point, 0.5f);
                }
            }
            yield return new WaitForFixedUpdate();
        }
        
        hookState = HookState.Aiming;
    }

    protected IEnumerator ShootAtTarget()
    {
        float startTime = Time.time;
        RaycastHit hit;

        Ray ray = new Ray(transform.position, laserEnd.position - transform.position);

        if(isElite) StartCoroutine(UseSkill());

        if(Physics.Raycast(ray, out hit, searchRange * 100, ~(1 << 11)))
        {
            laserEnd.position = hit.point;
        }

        while(Vector3.Distance(hit.point, transform.position) > 2f && Time.time - startTime < 5f)
        {
            hook.transform.LookAt(laserEnd);

            laserEnd.position = hit.point;

            m_rigidbody.velocity = (hit.point - transform.position).normalized * speed;
            yield return null;
        }

        laserEnd.position = laserStart.position;
        hookState = HookState.Inactive;
        nextHookState = HookState.Inactive;
    }

    protected IEnumerator UseSkill()
    {
        skillOne.Use();
        Pool.Spawn(shootBulletSound, transform.position, transform.rotation, transform);
        yield return new WaitForSeconds(0.25f);
    }

    protected virtual void OnTriggerStay(Collider other) 
    {
        if(other.gameObject.layer != 0) return;

        Entity entityHit;
        if((entityHit = other.gameObject.GetComponent<Entity>()) != null)
        {
            if(entityHit.faction != faction)
            {
                entityHit.TakeDamage(damagePerSec * Time.deltaTime);
            }
        }
    }
}