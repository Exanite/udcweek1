﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityAI : Entity
{
    [Header("EntityAI:")]
    public Entity target;
    public float searchRange = 10f;

    protected virtual bool GetTarget()
	{
		Collider[] colliders = Physics.OverlapSphere(transform.position, searchRange); //Get GameObjects in range

		Entity closestEnemy = null;
		float closestEnemyRange = Mathf.Infinity;

		foreach (Collider collider in colliders)
		{
			Entity otherEntity;
			if((otherEntity = collider.gameObject.GetComponent<Entity>()) != null) //Get GameObjects with Entity in range
			{
				if(otherEntity.faction != faction) //Get enemy faction(faction not equal to own)
				{
					if(Vector3.Distance(collider.gameObject.transform.position, transform.position) < closestEnemyRange) //Get closest enemy
					{
						closestEnemy = otherEntity;
						closestEnemyRange = Vector3.Distance(collider.gameObject.transform.position, transform.position);
					}
				}
			}
		}

		target = closestEnemy;

		return (closestEnemy != null);
	}

	protected IEnumerator UpdateAICoroutine(float repeatRate = 0.20f, float repeatRatevariation = 0.05f)
	{
        yield return new WaitForSeconds(Random.Range(-repeatRatevariation, repeatRatevariation));

		while(target == null)
		{
			GetTarget();
			yield return new WaitForSeconds(repeatRate);
		}
	}

    public override void OnDespawn()
    {
        target = null;
        searchRange = mOriginalPrefab.GetComponent<EntityAI>().searchRange;
    }
}