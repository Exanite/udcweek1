﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.Stats;
using Exanite.SkillSystem;
using Exanite.ObjectPooling;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(PoolInstanceID))]
[DisallowMultipleComponent]
public class Entity : MonoBehaviour, IPoolable
{
	[Header("Entity:")]
	public SkillSO currentSkill;
	public SkillHolder skillOne;
	public StatRegenerating health;
	public StatFaction faction = StatFaction.Unset;
	public StatHolder damage;
	public bool isDead = false;
	public float lastAttackTime = Mathf.NegativeInfinity;

	public List<Transform> skillSpawns;

	protected Rigidbody m_rigidbody;
	protected GameObject mOriginalPrefab;

	protected virtual void Start() 
	{
		m_rigidbody = GetComponent<Rigidbody>();
		m_rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;

		health = (health.Max.FinalValue == 0) ? new StatRegenerating(100f, 1f) : health;
		health.SetMax();

		mOriginalPrefab = GetComponent<PoolInstanceID>().originalPrefab;
		if(currentSkill != null) skillOne = new SkillHolder(currentSkill, this, skillSpawns.ToArray());
	}
	
	protected virtual void Update() 
	{
		health.Regenerate();
		if(!isDead && health.Value <= 0f && isActiveAndEnabled)
		{
			OnDeath();
			isDead = true;
		}
	}

	public virtual void TakeDamage(StatHolder amount, bool isHit = true)
	{
		TakeDamage(amount.FinalValue, isHit);
	}

	public virtual void TakeDamage(float amount, bool isHit = true)
	{
		health.Value -= amount;
		if(isHit) OnHit();
	}

	protected virtual void OnHit() { }

	protected virtual void OnDeath()
	{
		Pool.Despawn(gameObject);
	}

	public virtual void OnSpawn()
	{
		Entity originalPrefabEntity;
		if(mOriginalPrefab != null && (originalPrefabEntity = mOriginalPrefab.GetComponent<Entity>()) != null)
		{
			currentSkill = originalPrefabEntity.currentSkill;
			health.Max.RemoveAllModifiers();
			health.Regen.RemoveAllModifiers();
			health.SetMax();
			faction = originalPrefabEntity.faction;
			damage = originalPrefabEntity.damage;
			isDead = false;
		}
	}

	public virtual void OnDespawn()
	{
		Entity originalPrefabEntity = mOriginalPrefab.GetComponent<Entity>();
		currentSkill = originalPrefabEntity.currentSkill;
		health.Max.RemoveAllModifiers();
		health.Regen.RemoveAllModifiers();
		health.SetMax();
		faction = originalPrefabEntity.faction;
		damage = originalPrefabEntity.damage;
		isDead = false;
	}
}
