﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.ObjectPooling;

namespace Exanite.SkillSystem
{
	[System.Serializable]
	public class SkillHolder
	{
		public SkillSO skill;
		public Entity owner;
		public float lastUseTime = 0f;
		public List<Transform> spawnpoints = new List<Transform>();

		public SkillHolder(SkillSO skill, Entity owner, params Transform[] spawnpoints)
		{
			this.skill = skill;
			this.owner = owner;
			this.spawnpoints.Clear();
			this.spawnpoints.AddRange(spawnpoints);
		}

		public bool Use(bool ignoreAttackTime = false)
		{
			if(CanUse(ignoreAttackTime))
			{
				Spawn(owner);
				return true;
			}

			return false;
		}

		protected void Spawn(Entity owner)
		{
			owner.lastAttackTime = Time.time;
			lastUseTime = Time.time;

			foreach(Transform spawnpoint in spawnpoints)
			{
				GameObject spawnedSkill = Pool.Spawn(skill.prefab, spawnpoint.position, spawnpoint.rotation);
				SkillData spawnedSkillData = spawnedSkill.GetComponent<SkillData>();
				spawnedSkillData.owner = owner;
			}
		}

		protected bool CanUse(bool ignoreAttackTime = false)
		{
			if(!(Time.time - lastUseTime >= skill.cooldown)) return false; // If cooldown not ready

			if(Time.time - owner.lastAttackTime > skill.attackTime || ignoreAttackTime) return true; // If attack ready and cooldown ready OR is ignoring attackTime

			return false; // If cooldown is not ready as well as still attacking
		}
	}
}