﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.Stats;
using Exanite.ObjectPooling;

namespace Exanite.SkillSystem
{
	[RequireComponent(typeof(Rigidbody))]
	[RequireComponent(typeof(PoolInstanceID))]
	[RequireComponent(typeof(Collider))]
	[RequireComponent(typeof(SkillDespawner))]
	[DisallowMultipleComponent]
	public class SkillData : MonoBehaviour, IPoolable
	{
		public Entity owner;
		public StatFaction faction = StatFaction.Unset;
		public bool targetOwnFaction = false; // For buffs/heals
		public StatHolder damage;

		public Rigidbody mRigidbody;
		public GameObject mOriginalSkill;

		protected SkillDespawner m_skillDespawner;
		protected Collider m_collider;

		protected float m_timeSpawned;

		protected virtual void Start() 
		{
			mRigidbody = GetComponent<Rigidbody>();
			mOriginalSkill = GetComponent<PoolInstanceID>().originalPrefab;
			m_collider = GetComponent<Collider>();
			m_skillDespawner = GetComponent<SkillDespawner>();

			mRigidbody.constraints = RigidbodyConstraints.FreezeAll;
			m_collider.isTrigger = true;
			gameObject.layer = 11;
		}

		public virtual void OnSpawn()
		{
			if(!owner) Pool.Despawn(gameObject);

			faction = owner.faction;
			damage.AddModifiers(mOriginalSkill.GetComponent<SkillData>().damage.GetCombinedModifiers().ToArray());
			damage.AddModifiers(owner.damage.GetCombinedModifiers().ToArray());
		}

		public virtual void OnDespawn()
		{
			owner = null;
			faction = StatFaction.Unset;
			damage.Clear();
		}

		public virtual void SkillDespawn()
		{
			m_skillDespawner.SkillDespawn();
		}
	}
}