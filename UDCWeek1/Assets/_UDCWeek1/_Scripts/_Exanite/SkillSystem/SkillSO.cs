﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Exanite.SkillSystem
{
	[CreateAssetMenu(fileName = "SkillSO", menuName = "UDCWeek1/SkillData", order = 0)]
	public class SkillSO : ScriptableObject 
	{
		public Sprite icon;
		public GameObject prefab;

		public float attackTime = 1f;
		public float cooldown = 0f;
	}
}