﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Exanite.SkillSystem
{
	[RequireComponent(typeof(SkillData))]
	public class SkillComponent : MonoBehaviour 
	{
		protected SkillData m_skillData;

		protected virtual void Start()
		{
			m_skillData = GetComponent<SkillData>();
		}
	}
}