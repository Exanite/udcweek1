﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.ObjectPooling;

namespace Exanite.SkillSystem
{
	[DisallowMultipleComponent]
	public class SkillDespawner : MonoBehaviour 
	{
		public virtual void SkillDespawn()
		{
			Pool.Despawn(gameObject);
		}
	}
}