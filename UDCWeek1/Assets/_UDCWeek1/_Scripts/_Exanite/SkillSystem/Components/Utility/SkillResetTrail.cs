﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.ObjectPooling;

namespace Exanite.SkillSystem
{
	[RequireComponent(typeof(TrailRenderer))]
	public class SkillResetTrail : MonoBehaviour, IPoolable
	{
		protected TrailRenderer trail;
		protected float time;

		protected virtual void Start() 
		{
			trail = GetComponent<TrailRenderer>();
			time = trail.time;
		}

		public virtual void OnSpawn()
		{
			StartCoroutine(ResetTrailRenderer());
		}

		public virtual void OnDespawn() 
		{ 
		}

		protected virtual IEnumerator ResetTrailRenderer()
 		{
			trail.enabled = false;
			yield return null;
			trail.Clear();
			trail.enabled = true;
 		}
	}
}