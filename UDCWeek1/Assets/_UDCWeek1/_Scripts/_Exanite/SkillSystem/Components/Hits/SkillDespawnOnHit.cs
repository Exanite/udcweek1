﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.SkillSystem.Hits;

namespace Exanite.SkillSystem
{
	[DisallowMultipleComponent]
	public class SkillDespawnOnHit : SkillOnHit, IHit
	{
		public virtual void OnHit() 
		{
			m_skillData.SkillDespawn();
		}
	}
}