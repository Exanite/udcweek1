﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Exanite.SkillSystem.Hits
{
	public interface IHit
	{
		void OnHit();
	}
}

