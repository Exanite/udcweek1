﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.Stats;
using Exanite.SkillSystem.Hits;

namespace Exanite.SkillSystem
{
	[DisallowMultipleComponent]
	public class SkillHitHelper : SkillComponent
	{
		protected virtual void OnTriggerEnter(Collider other) 
		{
			if(other.gameObject.layer != 0) return;
			if(!m_skillData.targetOwnFaction && m_skillData.owner != null && other.gameObject == m_skillData.owner.gameObject) return;

			Entity entityHit;
			if((entityHit = other.gameObject.GetComponent<Entity>()) != null)
			{
				if(m_skillData.targetOwnFaction && entityHit.faction == m_skillData.faction || !m_skillData.targetOwnFaction && entityHit.faction != m_skillData.faction)
				{
					foreach(IHitTarget iHitTarget in GetComponents<IHitTarget>())
					{
						iHitTarget.OnHitTarget(entityHit);
					}
				}
			}

			foreach(IHit iHit in GetComponents<IHit>())
			{
				iHit.OnHit();
			}
		}
	}
}