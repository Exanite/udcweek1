﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.SkillSystem.Hits;

namespace Exanite.SkillSystem
{
	public class SkillDamageOnHit : SkillOnHit, IHitTarget
	{
		public bool countAsHit = true;

		public virtual void OnHitTarget(Entity target)
		{
			target.TakeDamage(m_skillData.damage, countAsHit);
		}
	}
}