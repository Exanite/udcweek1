﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.ObjectPooling;

namespace Exanite.SkillSystem
{
	public class SkillProjectile : SkillComponent, IPoolable
	{
		public float speed = 5f;
		public float distanceTraveled = 0f;

		protected virtual void FixedUpdate()
		{
			distanceTraveled += speed * Time.fixedDeltaTime;
			transform.position += transform.forward * speed * Time.fixedDeltaTime;
		}

		public virtual void OnSpawn() { }

		public virtual void OnDespawn()
		{
			distanceTraveled = 0f;
		}
	}
}