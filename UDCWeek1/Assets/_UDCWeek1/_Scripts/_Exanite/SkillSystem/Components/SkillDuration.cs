﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.ObjectPooling;

namespace Exanite.SkillSystem
{
	[DisallowMultipleComponent]
	public class SkillDuration : SkillComponent, IPoolable
	{
		public float duration = 10f;
		protected float m_timeSpawned = Mathf.Infinity;

		protected SkillDuration m_originalSkillDuration;

		protected override void Start() 
		{
			base.Start();

			m_originalSkillDuration = m_skillData.mOriginalSkill.GetComponent<SkillDuration>();
		}

		protected virtual void Update() 
		{
			if(Time.time - m_timeSpawned >= duration)
			{
				SkillDespawn();
			}
		}

		public virtual void OnSpawn()
		{
			m_timeSpawned = Time.time;
		}

		public virtual void OnDespawn()
		{
			duration = m_originalSkillDuration.duration;
			m_timeSpawned = Mathf.Infinity;
		}

		protected virtual void SkillDespawn()
		{
			m_skillData.SkillDespawn();
		}
	}
}