﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Exanite.ObjectPooling
{
	public class PoolInstanceID : MonoBehaviour 
	{
		public GameObject originalPrefab;
		public int instanceID;
	}
}