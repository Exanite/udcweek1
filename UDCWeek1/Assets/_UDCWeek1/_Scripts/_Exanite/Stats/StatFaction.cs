﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Exanite.Stats
{
	public enum StatFaction
	{
		Unset = 0, //Default faction
		Player = 10, //Player faction

		Gearworks = 20, //Gearworks faction
	}
}