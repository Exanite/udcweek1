﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetColor : MonoBehaviour 
{
	public Color color;
	private Color _color;

	private void Start() 
	{
		SetColors();
	}

	private void Update() 
	{
		if(color != _color)
		{
			SetColors();
		}
	}

	private void SetColors()
	{
		color.a = 1f;

		Renderer renderer = null;
		ParticleSystem particles = null;
		if((renderer = gameObject.GetComponent<Renderer>()) != null)
		{
			SetRendererColor(renderer);
		}
		if((particles = gameObject.GetComponent<ParticleSystem>()) != null)
		{
			SetParticleColor(particles);
		}
		foreach(Transform child in transform)
		{
			renderer = null;
			if((renderer = child.gameObject.GetComponent<Renderer>()) != null)
			{
				SetRendererColor(renderer);
			}
			if((particles = child.gameObject.GetComponent<ParticleSystem>()) != null)
			{
				SetParticleColor(particles);
			}
		}

		_color = color;			
	}

	private void SetRendererColor(Renderer renderer)
	{
		renderer.material.SetColor("_Color", color);
		renderer.material.SetColor("_EmissionColor", color);
	}

	private void SetParticleColor(ParticleSystem particles)
	{
		ParticleSystem.MainModule module= particles.main;
		module.startColor = color;
		particles.Clear();
		particles.Simulate(0.0f, true, true);
		particles.Play();
	}
}
