﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.ObjectPooling;

namespace Exanite.Utility
{
	public class SpawnPrefabOnStart : MonoBehaviour
	{
		public GameObject prefab;

		protected virtual void Start() 
		{
			Pool.Spawn(prefab, transform.position, transform.rotation);
		}
	}
}