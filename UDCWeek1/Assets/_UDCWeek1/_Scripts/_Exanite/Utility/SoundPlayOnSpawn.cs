﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.ObjectPooling;

namespace Exanite.Utility
{
	[RequireComponent(typeof(AudioSource))]
	public class SoundPlayOnSpawn : MonoBehaviour, IPoolable
	{
		public AudioClip soundEffect;

		protected AudioSource m_audioSource;

		protected virtual void Start() 
		{
			m_audioSource = GetComponent<AudioSource>();
			m_audioSource.playOnAwake = false;
			m_audioSource.Stop();
			m_audioSource.clip = soundEffect;
		}

		private void Update() 
		{
			if(!m_audioSource.isPlaying)
			{
				Pool.Despawn(gameObject);
			}
		}

		public virtual void OnSpawn()
		{
			m_audioSource.Play();
		}

		public virtual void OnDespawn()
		{
			m_audioSource.Stop();
		}
	}
}