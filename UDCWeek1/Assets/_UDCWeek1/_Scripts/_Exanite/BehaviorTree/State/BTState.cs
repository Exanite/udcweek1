﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Exanite.BehaviorTree
{
	[System.Serializable]
	public enum BTState
	{
		Failed = 0,
		Succeeded = 1,
		Running = 2,
		NotStarted = 3,
	}
}